package com.example.gateway;

import com.example.gateway.utils.KeycloakToken;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static com.example.gateway.utils.ANSIColors.*;

@Disabled
class KeycloakTest {

    @Test
    void getAccessTokenSuccess() {
        KeycloakToken token = KeycloakToken.acquire(
                        "http://localhost:8080/auth/",
                        "qt3music",
                        "qt3music-client",
                        "user@gmail.com",
                        "user@gmail.com")
                .block();

        printBlue(token);
        printGreen(token.getAccessToken());
    }

}
